from turtle import title

from selenium.webdriver.common.by import By


class MainPageLocators(object):
    """A class for main page locators. All main page locators should come here"""

    GO_BUTTON = (By.ID, 'submit')
    INPUT_TEXT = (By.ID, 'heights')
    H1_TITLE = (By.TAG_NAME, 'h1')
    H2_TITLE = (By.TAG_NAME, 'h2')
    FORM_TITLE = (By.TAG_NAME, 'label')


class ResultsPageLocators(object):
    """A class for search results locators. All search results locators should
    come here"""
    RESULT = (By.ID, 'result')
    BAD_REQUEST = (By.TAG_NAME, 'h1')


class InputTextElementLocators(object):
    """A class for main page locators. All main page locators should come here"""
    pass
