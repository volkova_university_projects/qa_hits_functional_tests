from page_objects.locators import MainPageLocators, ResultsPageLocators


class BasePage(object):
    """Base class to initialize the base page that will be called from all
    pages"""

    def __init__(self, driver):
        self.driver = driver


class MainPage(BasePage):
    """Home page action methods come here. I.e. Python.org"""
    def input_data_in_text_field(self, heights):
        input_text_element = self.driver.find_element(*MainPageLocators.INPUT_TEXT)
        input_text_element.send_keys(heights)

    def is_title_matches(self):
        """Verifies that the hardcoded text "Python" appears in page title"""

        return "Maximum Area of Histogram" in self.driver.title

    def check_h1_title(self, expected):
        element = self.driver.find_element(*MainPageLocators.H1_TITLE)
        return element.text == expected

    def check_h2_title(self, expected):
        element = self.driver.find_element(*MainPageLocators.H2_TITLE)
        return element.text == expected


    def click_go_button(self):
        """Triggers the search"""

        element = self.driver.find_element(*MainPageLocators.GO_BUTTON)
        element.click()

    def check_name_go_button(self, expected):
        element = self.driver.find_element(*MainPageLocators.GO_BUTTON)
        return element.text == expected

    def check_form_title(self, expected):
        element = self.driver.find_element(*MainPageLocators.FORM_TITLE)
        return element.text == expected


class CheckResultsPage(BasePage):
    """Search results page action methods come here"""

    def is_results_found(self):
        # Probably should search for this text in the specific page
        # element, but as for now it works fine
        element = self.driver.find_element(*ResultsPageLocators.RESULT)
        return element

    def check_result_equals(self, expected):
        # Probably should search for this text in the specific page
        # element, but as for now it works fine
        element = self.driver.find_element(*ResultsPageLocators.RESULT)
        return element.text == str(expected)

    def check_error_contains(self, expected):
        element = self.driver.find_element(*ResultsPageLocators.BAD_REQUEST)
        return element.text == expected

    def is_error_contains(self, expected):
        element = self.driver.find_element(*ResultsPageLocators.BAD_REQUEST)
        return element




