from integration_tests.common import TestSuite
from lib.apiwarpper import *


class TestInputValues(TestSuite):
    def test_post_empty_list_input_status(self):
        assert post_form(heights='').status_code == 400

    def test_post_single_element_input_status(self):
        assert post_form(heights='1').status_code == 400

    def test_post_valid_input_values_status(self):
        assert post_form(heights='1 2 3 4 2 3 4').status_code == 200

    def test_post_invalid_string_input_status(self):
        assert post_form(heights='some words').status_code == 400

    def test_post_invalid_float_input_status(self):
        heights_lists = [
            '1.12 345.12435 2.02 2345.234 0.13',
            '-10.21 -0.12 -1120.67867 -567.16678 -3455.1432',
            '0.00001 0.00002 0.00000001 0.000000001 0.0000003',
            '-0.00001 -0.00002 -0.00000001 -0.000000001 -0.0000003'
        ]
        for item in heights_lists:
            with self.subTest(item=item):
                assert post_form(heights=item).status_code == 400

    def test_post_valid_input_values_near_left_border_range_status(self):
        assert post_form(heights='1 1 1 1 1').status_code == 200

    def test_post_valid_input_values_near_right_border_range_status(self):
        assert post_form(heights='9999 9999 9999 9999 9999').status_code == 200

    def test_post_valid_input_values_in_the_middle_of_range_status(self):
        assert post_form(heights='5_000 4_999 4_999 4_999 5_000').status_code == 200

    def test_post_valid_input_values_equals_to_left_border_range_status(self):
        assert post_form(heights='0 0 0 0 0').status_code == 200

    def test_post_valid_input_values_equals_to_right_border_range_status(self):
        assert post_form(heights='10000 10000 10000 10000 10000').status_code == 200

    def test_post_invalid_input_value_before_left_then_left_border_range_status(self):
        assert post_form(heights='-1 -1 -1 -1 -1').status_code == 400

    def test_post_invalid_input_values_after_right_then_right_border_range_status(self):
        assert post_form(heights='10001 10001 10001 10001 10001').status_code == 400
