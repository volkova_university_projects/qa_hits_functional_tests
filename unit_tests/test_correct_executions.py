import random
from unit_tests.common import TestSuite


class CorrectExecution(TestSuite):
    def test_invalid_empty_list(self):
        heights = []
        with self.assertRaises(ValueError):
            self.solution.max_area(heights)

    def test_invalid_single_element(self):
        heights = [1]
        with self.assertRaises(ValueError):
            self.solution.max_area(heights)

    def test_two_random_elements_from_0_to_10000(self):
        m, n = random.randint(0, 10_000), random.randint(0, 10_000)
        heights = [m, n]
        expected = min(heights)
        result = self.solution.max_area(heights)
        self.assertEqual(result, expected)

    def test_two_equal_elements_from_0_to_10000(self):
        m = random.randint(0, 10_000)
        n = m
        heights = [m, n]
        result = self.solution.max_area(heights)
        self.assertEqual(result, m)

    def test_100000_equal_elements(self):
        m = random.randint(0, 10_000)
        heights = [m] * 100_000
        result = self.solution.max_area(heights)
        self.assertEqual(result, sum(heights) - m)

    def test_max_height_at_the_beginning(self):
        heights = [10, 8, 6, 5, 4, 3, 2, 1]
        result = self.solution.max_area(heights)
        self.assertEqual(result, 16)

    def test_max_height_in_the_middle(self):
        heights = [1, 8, 6, 15, 8, 5, 3]
        result = self.solution.max_area(heights)
        self.assertEqual(result, 24)

    def test_max_height_at_the_end(self):
        heights = [1, 2, 3, 4, 5, 6, 8, 10]
        result = self.solution.max_area(heights)
        self.assertEqual(result, 16)

    def test_mixed_large_heights(self):
        heights = [1, 2, 3, 9999, 5, 6, 1222, 100]
        result = self.solution.max_area(heights)
        self.assertEqual(result, 3666)

    def test_with_zero_heights(self):
        heights = [0, 0, 0, 0, 0, 0, 0]
        result = self.solution.max_area(heights)
        self.assertEqual(result, 0)

    def test_invalid_more_large_heights_then_10000(self):
        heights = [random.randrange(10_000, 20_000) for _ in range(100)]
        expected = []
        self.assertNotEqual(heights, expected)
