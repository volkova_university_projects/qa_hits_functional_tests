from unit_tests.common import TestSuite


class TestValidValuesInList(TestSuite):
    def test_valid_equal_values_near_left_border_range(self):
        heights = [1, 1, 1, 1, 1]
        expected = 1 * (len(heights) - 1)
        result = self.solution.max_area(heights)
        self.assertEqual(result, expected)

    def test_valid_equal_values_near_right_border_range(self):
        heights = [9_999, 9_999, 9_999, 9_999, 9_999]
        expected = 9_999 * (len(heights) - 1)
        result = self.solution.max_area(heights)
        self.assertEqual(result, expected)

    def test_valid_values_in_the_middle_of_range(self):
        heights = [5_000, 4_999, 4_999, 4_999, 5_000]
        expected = 5_000 * (len(heights) - 1)
        result = self.solution.max_area(heights)
        self.assertEqual(result, expected)

    def test_valid_values_equals_to_left_border_range(self):
        heights = [0, 0, 0, 0, 0]
        expected = 0
        result = self.solution.max_area(heights)
        self.assertEqual(result, expected)

    def test_valid_values_equals_to_right_border_range(self):
        heights = [10_000, 10_000, 10_000, 10_000, 10_000]
        expected = 10_000 * (len(heights) - 1)
        result = self.solution.max_area(heights)
        self.assertEqual(result, expected)

    def test_invalid_value_before_left_then_left_border_range(self):
        heights = [-1, -1, -1, -1, -1]
        with self.assertRaises(ValueError):
            self.solution.max_area(heights)

    def test_invalid_values_after_right_then_right_border_range(self):
        heights = [10_001, 10_001, 10_001, 10_001, 10_001]
        with self.assertRaises(ValueError):
            self.solution.max_area(heights)

    def test_invalid_float_values_parametrized(self):
        heights_lists = [
            [1.12, 345.12435, 2.02, 2345.234, 0.13],
            [-10.21, -0.12, -1120.67867, -567.16678, -3455.1432],
            [0.00001, 0.00002, 0.00000001, 0.000000001, 0.0000003],
            [-0.00001, -0.00002, -0.00000001, -0.000000001, -0.0000003]
        ]
        for item in heights_lists:
            with self.subTest(item=item):
                self.assertListEqual(item, item)

