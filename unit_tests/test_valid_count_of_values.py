import random

from unit_tests.common import TestSuite


class ValidCountOfValues(TestSuite):
    def test_valid_count_of_values_near_left_border_range(self):
        heights = [1, 2, 3]
        heights_length = len(heights)
        self.assertEqual(heights_length, 3)

    def test_valid_count_of_values_near_right_border_range(self):
        heights = [random.randrange(0, 10_000) for _ in range(99_999)]
        heights_length = len(heights)
        self.assertEqual(heights_length, 99_999)

    def test_valid_count_of_values_in_the_middle_of_range(self):
        middle = 100_000//2
        heights = [random.randrange(0, 10_000) for _ in range(middle)]
        heights_length = len(heights)
        self.assertEqual(heights_length, middle)

    def test_valid_count_of_values_equals_left_border_range(self):
        left_border = 2
        heights = [random.randrange(0, 10_000) for _ in range(left_border)]
        heights_length = len(heights)
        self.assertEqual(heights_length, left_border)

    def test_valid_count_of_values_equals_right_border_range(self):
        right_border = 100_000
        heights = [random.randrange(0, 10_000) for _ in range(right_border)]
        heights_length = len(heights)
        self.assertEqual(heights_length, right_border)

    def test_invalid_count_of_values_after_left_then_left_border_range(self):
        left_border = 2
        heights = [random.randrange(0, 10_000) for _ in range(left_border - 1)]
        with self.assertRaises(ValueError):
            self.solution.max_area(heights)

    def test_invalid_count_of_values_after_right_then_right_border_range(self):
        right_border = 100_000
        heights = [random.randrange(0, 10_000) for _ in range(right_border + 1)]
        with self.assertRaises(ValueError):
            self.solution.max_area(heights)

    def test_invalid_count_of_values_more_then_right_border(self):
        right_border = 100_000
        heights = [random.randrange(0, 10_000) for _ in range(right_border + 10_000)]
        with self.assertRaises(ValueError):
            self.solution.max_area(heights)
