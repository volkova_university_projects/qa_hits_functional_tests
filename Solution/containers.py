# from typing import List


class Solution:
    def max_area(self, heights):
        for item in heights:
            if not isinstance(item, int) or item < 0 or item > 10_000:
                raise ValueError


        if 2 <= len(heights) <= 100_000:

            left_idx, right_idx = 0, len(heights) - 1
            max_area = 0
            max_height = max(heights)

            while left_idx < right_idx:
                current_area = min(heights[left_idx], heights[right_idx]) * (right_idx - left_idx)
                max_area = max(max_area, current_area)
                if heights[left_idx] < heights[right_idx]:
                    left_idx += 1
                else:
                    right_idx -= 1

                if (right_idx - left_idx) * max_height <= max_area:
                    break

            return max_area
        raise ValueError


if __name__ == '__main__':
    result = Solution()
    result.max_area([1.4, 1, 1, 1, 1])
