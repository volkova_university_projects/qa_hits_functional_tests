from typing import Any

from flask import Flask, request, render_template, abort
from Solution.containers import Solution

app = Flask(__name__, template_folder='template')
solution = Solution()


@app.route('/', methods=['GET', 'POST'])
def index():
    # global max_area
    if request.method == 'POST':
        heights = list(request.form['heights'].split(' '))
        try:
            heights = list(map(int, heights))
            max_area = solution.max_area(heights)
        except:
            abort(400)
        return render_template('index.html', max_area=max_area)
    else:
        return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
