import requests

BASE_URL = 'http://localhost:5000/'


def get_form():
    response = requests.get(BASE_URL)
    return response


def post_form(heights):
    post_data = {'heights': heights}
    response = requests.post(BASE_URL, data=post_data)
    return response
