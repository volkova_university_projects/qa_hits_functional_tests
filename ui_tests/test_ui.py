from page_objects import page
from page_objects.page import CheckResultsPage


class TestUISelenium():
    def test_main_page_title(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)

        assert main_page.is_title_matches()

    def test_main_page_h1_title(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)

        assert main_page.check_h1_title('Maximum Area of Histogram')

    def test_main_page_h2_title(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)

        assert main_page.check_h2_title('Maximum Area:')

    def test_main_page_name_go_button(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)

        assert main_page.check_name_go_button('Calculate Maximum Area')

    def test_main_page_form_title(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)

        assert main_page.check_form_title('Enter the heights of the histogram (space-separated):')

    def test_main_page_check_valid_numbers(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)
        main_page.input_data_in_text_field("1 2 3 4 2 3 4")
        main_page.click_go_button()
        check_results_page = CheckResultsPage(browser)

        assert check_results_page.check_result_equals(12)

    def test_main_page_has_result_container(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)
        main_page.input_data_in_text_field("1 2 3 4 2 3 4")
        main_page.click_go_button()
        check_results_page = CheckResultsPage(browser)

        assert check_results_page.is_results_found()

    def test_main_page_check_invalid_numbers(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)
        main_page.input_data_in_text_field("-1 -1 -1 -1 -1")
        main_page.click_go_button()
        check_results_page = CheckResultsPage(browser)

        assert check_results_page.check_error_contains('Bad Request')

    def test_main_page_has_error_invalid_numbers(self, browser):
        browser.get('http://localhost:5000/')
        main_page = page.MainPage(browser)
        main_page.input_data_in_text_field("-1 -1 -1 -1 -1")
        main_page.click_go_button()
        check_results_page = CheckResultsPage(browser)

        assert check_results_page.is_error_contains

